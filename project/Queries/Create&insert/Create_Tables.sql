CREATE TABLE person 
  ( 
     person_id  VARCHAR2(10), 
     last_name  VARCHAR2(30) NOT NULL, 
     first_name VARCHAR2(30), 
     gender     VARCHAR2(10), 
     email      VARCHAR2(30), 
     PRIMARY KEY (person_id) 
  ); 

CREATE TABLE phone_number 
  ( 
     phone_id   VARCHAR(10), 
     phone_type VARCHAR(20), 
     phone_num  VARCHAR(14) NOT NULL, 
     PRIMARY KEY (phone_id) 
  ); 

CREATE TABLE person_phone 
  ( 
     person_id VARCHAR(10), 
     phone_id  VARCHAR(10), 
     PRIMARY KEY (person_id, phone_id), 
     FOREIGN KEY (person_id) REFERENCES person, 
     FOREIGN KEY (phone_id) REFERENCES phone_number 
  ); 

CREATE TABLE address 
  ( 
     address_id     VARCHAR(10), 
     address_type   VARCHAR(20), 
     street_address VARCHAR(30) NOT NULL, 
     city           VARCHAR(30), 
     state          VARCHAR(30), 
     zipcode        VARCHAR(5) NOT NULL, 
     PRIMARY KEY (address_id) 
  ); 

CREATE TABLE company 
  ( 
     company_id     VARCHAR2(50), 
     company_name   VARCHAR2(100), 
     primary_sector VARCHAR2(100), 
     website        VARCHAR2(100), 
     PRIMARY KEY (company_id) 
  ); 

CREATE TABLE company_address 
  ( 
     company_id VARCHAR(10), 
     address_id VARCHAR(10), 
     PRIMARY KEY (company_id, address_id), 
     FOREIGN KEY (company_id) REFERENCES company, 
     FOREIGN KEY (address_id) REFERENCES address 
  ); 

CREATE TABLE person_address 
  ( 
     person_id VARCHAR(10), 
     address_id  VARCHAR(10), 
     PRIMARY KEY (person_code, address_id), 
     FOREIGN KEY (person_code) REFERENCES person, 
     FOREIGN KEY (address_id) REFERENCES address 
  ); 

CREATE TABLE company_phone 
  ( 
     company_id VARCHAR(10), 
     phone_id   VARCHAR(10), 
     PRIMARY KEY (company_id, phone_id), 
     FOREIGN KEY (company_id) REFERENCES company, 
     FOREIGN KEY (phone_id) REFERENCES phone_number 
  ); 

CREATE TABLE specialty 
  ( 
     specialty_id   VARCHAR(10), 
     specialty_name VARCHAR(30) NOT NULL, 
     PRIMARY KEY (specialty_id) 
  ); 

CREATE TABLE company_specialty 
  ( 
     company_id   VARCHAR(10), 
     specialty_id VARCHAR(10), 
     PRIMARY KEY (company_id, specialty_id), 
     FOREIGN KEY (company_id) REFERENCES company, 
     FOREIGN KEY (specialty_id) REFERENCES specialty 
  ); 

CREATE TABLE job_profile 
  ( 
     job_profile_id          VARCHAR(10), 
     job_profile_title       VARCHAR(50) NOT NULL, 
     job_profile_description VARCHAR(300), 
     PRIMARY KEY (job_profile_id) 
  ); 

CREATE TABLE job 
  ( 
     job_id         VARCHAR(10), 
     job_profile_id VARCHAR(10) NOT NULL, 
     company_id     VARCHAR(10) NOT NULL, 
     job_type       VARCHAR(10), 
     pay_rate       NUMERIC(8, 2), 
     pay_type       VARCHAR(6), 
     PRIMARY KEY (job_id), 
     FOREIGN KEY (job_profile_id) REFERENCES job_profile, 
     FOREIGN KEY (company_id) REFERENCES company 
  ); 

CREATE TABLE skill 
  ( 
     skill_id          VARCHAR(10), 
     skill_name        VARCHAR(30) NOT NULL, 
     skill_description VARCHAR(300), 
     skill_level       VARCHAR(8), 
     PRIMARY KEY (skill_id) 
  ); 

CREATE TABLE job_skill 
  ( 
     job_id   VARCHAR(10), 
     skill_id VARCHAR(10), 
     PRIMARY KEY (job_id, skill_id), 
     FOREIGN KEY (job_id) REFERENCES job, 
     FOREIGN KEY (skill_id) REFERENCES skill 
  ); 
  
CREATE TABLE person_skill 
  ( 
     person_id VARCHAR(10), 
     skill_id  VARCHAR(10), 
     PRIMARY KEY (person_id, skill_id), 
     FOREIGN KEY (person_id) REFERENCES person, 
     FOREIGN KEY (skill_id) REFERENCES skill 
  ); 
  
CREATE TABLE job_profile_skill 
  ( 
     job_profile_id VARCHAR(10), 
     skill_id       VARCHAR(10), 
     PRIMARY KEY (job_profile_id, skill_id), 
     FOREIGN KEY (job_profile_id) REFERENCES job_profile, 
     FOREIGN KEY (skill_id) REFERENCES skill 
  ); 

CREATE TABLE job_history 
  ( 
     person_id  VARCHAR(10), 
     job_id     VARCHAR(10), 
     start_date DATE NOT NULL, 
     end_date   DATE, 
     PRIMARY KEY (person_id, job_id), 
     FOREIGN KEY (person_id) REFERENCES person, 
     FOREIGN KEY (job_id) REFERENCES job 
  ); 

CREATE TABLE course 
  ( 
     course_id          VARCHAR(10), 
     course_title       VARCHAR(50) NOT NULL, 
     course_level       VARCHAR(8), 
     course_description VARCHAR(300), 
     status             VARCHAR(8), 
     retail_price       NUMERIC(6, 2), 
     PRIMARY KEY (course_id) 
  ); 

CREATE TABLE format 
  ( 
     format_id   VARCHAR(10), 
     format_name VARCHAR(20) NOT NULL, 
     PRIMARY KEY (format_id) 
  ); 

CREATE TABLE section 
  ( 
     course_id  VARCHAR(10), 
     section_no VARCHAR(10), 
     year       NUMERIC(4, 0), 
     format_id  VARCHAR(10), 
     cost       NUMERIC(6, 2), 
     PRIMARY KEY (course_id, section_no, year), 
     FOREIGN KEY (course_id) REFERENCES course, 
     FOREIGN KEY (format_id) REFERENCES format 
  ); 

CREATE TABLE course_skill 
  ( 
     course_id VARCHAR(10), 
     skill_id  VARCHAR(10), 
     PRIMARY KEY (course_id, skill_id), 
     FOREIGN KEY (course_id) REFERENCES course, 
     FOREIGN KEY (skill_id) REFERENCES skill 
  ); 

CREATE TABLE taken 
  ( 
     course_id      VARCHAR(10), 
     section_no     VARCHAR(10), 
     year           NUMERIC(4, 0), 
     person_id      VARCHAR(10), 
     completed_date DATE, 
     PRIMARY KEY (course_id, section_no, year, person_id), 
     FOREIGN KEY (course_id, section_no, year) REFERENCES section, 
     FOREIGN KEY (person_id) REFERENCES person 
  ); 

CREATE TABLE offers 
  ( 
     course_id  VARCHAR(10), 
     section_no VARCHAR(10), 
     year       NUMBER(4, 0), 
     company_id VARCHAR(10), 
     PRIMARY KEY (course_id, section_no, year, company_id), 
     FOREIGN KEY (course_id, section_no, year) REFERENCES section, 
     FOREIGN KEY (company_id) REFERENCES company 
  ); 

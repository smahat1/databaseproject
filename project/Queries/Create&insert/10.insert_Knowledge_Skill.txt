Insert into Knowledge_Skill (ks_code, title, description, ks_level, c_code) values ('KS601','data modeling',null,'medium','CSCI4125');
Insert into Knowledge_Skill (ks_code, title, description, ks_level, c_code) values ('KS602','relational models',null,'medium','CSCI4125');
Insert into Knowledge_Skill (ks_code, title, description, ks_level, c_code) values ('KS603','SQL',null,'medium','CSCI4125');
Insert into Knowledge_Skill (ks_code, title, description, ks_level, c_code) values ('KS604','JDBC programming',null,'beginner','CSCI4125');
Insert into Knowledge_Skill (ks_code, title, description, ks_level, c_code) values ('KS605','RDBMS design',null,'medium','CSCI4125');
Insert into Knowledge_Skill (ks_code, title, description, ks_level, c_code) values ('KS606','driving',null,'medium','CSCI4128');